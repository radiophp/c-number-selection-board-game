//Final Game Code
#include <cstdlib> 
#include <iostream> 
#include <conio.h>
#include <string.h>
#include <time.h>

#define RESET   "\033[0;0m"
#define BLACK   "\033[0;30m"      /* Black */				 
#define RED     "\033[0;31m"      /* Red */
#define GREEN   "\033[0;32m"      /* Green */
#define YELLOW  "\033[0;33m"      /* Yellow */
#define BLUE    "\033[0;34m"      /* Blue */
#define MAGENTA "\033[0;35m"      /* Magenta */
#define CYAN    "\033[0;36m"      /* Cyan */
#define WHITE   "\033[0;37m"      /* White */
#define BACKBLACK   "\033[0;40m"      /* BACK Black */
#define BACKRED     "\033[0;41m"      /* BACK Red */
#define BACKGREEN   "\033[0;42m"      /* BACK Green */
#define BACKYELLOW  "\033[0;43m"      /* BACK Yellow */
#define BACKBLUE    "\033[0;44m"      /* BACK Blue */
#define BACKMAGENTA "\033[0;45m"      /* BACK Magenta */
#define BACKCYAN    "\033[0;46m"      /* BACK Cyan */
#define BACKWHITE   "\033[0;47m"      /* BACK White */

#define GAMESIZE 10
int AisComputer, BisComputer;
int board[GAMESIZE][GAMESIZE];
bool used[GAMESIZE][GAMESIZE];
int scoreA = 0, scoreB = 0;
int jokerA, jokerB;
bool turn = 0; // 0 is Player A, 1 is Player B
int selectible;
char ErrorMessage[100];
char AutoPlayer();

bool isSelectible(int i, int j);


void SetErrorMessage(char* str)
{
	strcpy(ErrorMessage, str);	
}

void Initiate()
{
	srand(time(NULL));
	for(int i=0; i<GAMESIZE; i++)
		for(int j=0; j<GAMESIZE; j++)
		{		
			board[i][j] = rand() % 100 - 50;
			if(board[i][j] >= 0) board[i][j] +=1;
			used[i][j] = false;
		}
	selectible = GAMESIZE / 2;
	SetErrorMessage("");
	jokerA = jokerB = GAMESIZE / 3;
	
}

void DrawLine()
{
	printf("       --------------------------------------------------------\n");
}

void PrintBoard()
{
	printf("       %s", !turn ? YELLOW : "" );	
	for(int i=0; i<GAMESIZE; i++)
		printf("     %c ", 65+i);
	printf("\n"RESET);
	DrawLine();
	
	for(int i=0; i<GAMESIZE; i++)
	{	
		printf("    %s%c: "RESET, turn ? YELLOW : "", 65+i);
		for(int j=0; j<GAMESIZE; j++)
			if(isSelectible(i, j))
			{				
				if(!used[i][j])
					printf(YELLOW"[%5d]"RESET, board[i][j]);
				else
					printf("       ");
			}
			else
			{				
				if(!used[i][j])
					printf("%6d ", board[i][j]);
				else
					printf("       ");
			}
		printf("\n");
	}
}

bool isEmpty(int rowcol)
{
	if (!turn)
	{
		for(int i=0; i<GAMESIZE; i++)
		   if(!used[rowcol][i]) return false;
	}
	else
	{
		for(int i=0; i<GAMESIZE; i++)
		   if(!used[i][rowcol]) return false;
	}
	 
	return true;
}


void PrintScores()
{
	printf("\n %c [%c] Player A: %5d   ", !turn ? '*' : ' ', AisComputer ? 'C' : 'H' ,scoreA);
	for(int i=0; i<jokerA; i++) printf("J");
		
	printf("\n %c [%c] Player B: %5d   ", turn ? '*' : ' ', BisComputer ? 'C' : 'H', scoreB);
	for(int i=0; i<jokerB; i++) printf("J");
	printf("\n\n");	
	DrawLine();
	printf(" *: Joker\n");
	printf(" X: Exit\n");
	
	printf(BACKRED"%s \n"RESET, ErrorMessage);
	printf("Select A row or column\n");	
	DrawLine();
}

bool isSelectible(int i, int j)
{
	if (!turn) 
		return (i == selectible); // && (!used[i][j])
	else
		return (j == selectible); // && (!used[i][j]);
}

void changeSelectible()
{
	int nonempty[GAMESIZE];
	int last=0;
	
	for(int i=0;i<GAMESIZE;i++)
		if(!isEmpty(i)) nonempty[last++]=i;
	
	selectible = nonempty[rand() % last];
}

void SelectPlayers()
{
	char c;
	system("cls");
	printf("Select Player A: (H)uman or (C)omputer: ");
	c=toupper(getch());
	AisComputer = c=='C';
	
	printf("\nSelect Player B: (H)uman or (C)omputer: ");
	c=toupper(getch());
	BisComputer = c=='C';
}

void MainLoop()
{
	char c = 0;
	while(c != 'X')
	{
		system("cls");
		PrintBoard();
		PrintScores();
		if(isEmpty(selectible))
		{
			printf(YELLOW"Game Over\n");
			if (scoreA > scoreB)
				printf("Player A WIN!!");
			else if (scoreB > scoreA)
				printf("Player B WIN!!");
			else // ==
				printf("Draw Game!");
			printf(RESET);
			break;			
		}
		
		if(!turn) 
			c=AisComputer ? AutoPlayer() : toupper( getch() );			
		else
			c=BisComputer ? AutoPlayer() : toupper( getch() );
					
		if(c=='X') break;
		if(c=='*')
		{
			SetErrorMessage("");
			if(!turn)
			{
				if(jokerA==0)
				{
					SetErrorMessage("You are out of Jokers");
					continue;
				}
				jokerA--;
			}
			else
			{
				if(jokerB==0)
				{
					SetErrorMessage("You are out of Jokers");
					continue;
				}
				jokerB--;
			}
			turn = !turn;			
			changeSelectible();
			continue;
		}
		if((c<65) || (c>64+GAMESIZE) )
		{
			SetErrorMessage("Select a row or coloumn in range");
			continue;
		}
		int idx = c-65;	
		//         for Player A								for Player B
		if(( !turn && used[selectible][idx] ) || ( turn && used[idx][selectible] ))
		{
				SetErrorMessage("This cell is already used. Please select another cell");
				continue;	
		}
		
				
		if(!turn)	// Player A
		{
			// Add the score
			scoreA += board[selectible][idx];
			// Delete Number
			used[selectible][idx] = true;
		}
		else		// Player B
		{
			// Add the score
			scoreB += board[idx][selectible];
			// Delete Number
			used[idx][selectible] = true;
		}
		// assign then new selection coloumn
		selectible = idx;
		SetErrorMessage("");
		turn = !turn;
	}
}


int main(int argc, char** argv) {
	SelectPlayers();
	Initiate();	
	MainLoop();
	return 0;
}
//Mohammad Inanloo Tayefe Yaghmorloo
//B2210.03313
char AutoPlayer() {
	/**
	 * AutoPlayer Function
	 * Developed by Mohammad Inanloo Tayefe Yaghmorloo, Student Number B2210.03313
	 * 
	 * Project Repository: https://gitlab.com/radiophp/c-number-selection-board-game
	 *
	 * The AutoPlayer function is designed for a board game, where it automatically selects the best possible move
	 * for the current player (either A or B). The function uses a sophisticated algorithm to analyze the game board
	 * and make strategic decisions.
	 *
	 * Algorithm:
	 * 1. Initialization: Establish initial variables for best score, best move, joker usage, and count the number 
	 *    of moves already made.
	 * 
	 * 2. Move Evaluation: For each potential move, the function checks whether the cell is available and evaluates
	 *    the potential score of making that move.
	 * 
	 *    - For Player A (row selector), it evaluates each column of the selectable row.
	 *    - For Player B (column selector), it evaluates each row of the selectable column.
	 * 
	 *    The function also estimates the best possible response of the opponent to the move and stores these values.
	 * 
	 * 3. Decision Making: The function iterates through all evaluated moves, prioritizing moves with positive scores.
	 *    It calculates a net score for each move by considering the opponent's potential response, aiming to maximize
	 *    the player's score while minimizing the opponent's opportunities.
	 * 
	 *    If no ideal move (net score is significantly positive) is found, it selects the move with the highest available
	 *    score.
	 * 
	 * 4. Joker Strategy: The function employs a strategic use of the Joker (a special move), particularly when the
	 *    opponent is significantly ahead, a high-value move is available, and the game is close to the end.
	 * 
	 * 5. Random Selection as Fallback: If no move stands out as the best, the function will randomly select from
	 *    the available moves. This ensures that the function always returns a valid move, maintaining the flow of the game.
	 * 
	 * This algorithmic approach allows for a dynamic and adaptive strategy, making the AutoPlayer function
	 * a competent player in the board game.
	 */
	
    int bestScore = -999;
    int bestMove = -1;
    bool shouldUseJoker = false;
    
	// Count the number of moves already made or numbers left on the board
    int movesMade = 0;
    for (int i = 0; i < GAMESIZE; i++) {
        for (int j = 0; j < GAMESIZE; j++) {
            if (used[i][j]) {
                movesMade++;
            }
        }
    }
    
	const int maxMoves = GAMESIZE; // Assuming GAMESIZE is the maximum number of moves
    int moveIndex[maxMoves];
    int scoreAfterMove[maxMoves];
    int opponentResponseScore[maxMoves];
    int numMoves = 0;

    // Evaluate all potential moves and their immediate scores
    for (int i = 0; i < GAMESIZE; i++) {
     
        bool cellIsAvailable = (!turn) ? (!used[selectible][i] && isSelectible(selectible, i)) 
                               : (!used[i][selectible] && isSelectible(i, selectible));
        
        if (cellIsAvailable) {
            int potentialScore = (!turn) ? board[selectible][i] : board[i][selectible];
            int opponentBestResponse = 0;
		//	printf("\nselectible %d used[selectible][i]  %d %d",selectible, i, used[selectible][i]);
            // Estimate the opponent's best response for this move
            for (int j = 0; j < GAMESIZE; j++) {
                if (!turn && !used[j][i]) {
                    // For Player A, check the column for the opponent's best move
                   
                        opponentBestResponse = std::max(opponentBestResponse, board[j][i]);
                 
                } else if(turn && !used[i][j]) {
                    // For Player B, check the row for the opponent's best move
                     
                        opponentBestResponse = std::max(opponentBestResponse, board[i][j]);
                  
                }
            }

            moveIndex[numMoves] = i;
            scoreAfterMove[numMoves] = potentialScore;
            opponentResponseScore[numMoves] = opponentBestResponse;
            numMoves++;
        }    	
    }

    // Evaluate each move option
	for (int i = 0; i < numMoves; i++) {
		//	printf("\nCheck movessss %d numMoves %d",scoreAfterMove[i],numMoves);
	    if (scoreAfterMove[i] > 0) { // Prioritize positive scores
	        int netScore = scoreAfterMove[i] - (opponentResponseScore[i] / 2); // Lessen the impact of opponent's response
	       // Print decision-making information for debugging
        //	printf("\nMove Index: %d, Potential Score: %d, Opponent Response: %d, Net Score: %d", moveIndex[i], scoreAfterMove[i], opponentResponseScore[i], netScore);
	        if (netScore > bestScore) {
	            bestScore = netScore;
	            bestMove = moveIndex[i];
	    //        printf(" --> New Best Move\n");
	        }else {
          //  	printf("\n");
        	}
	    }
	}
	
	// If there is no best move just use most avilable score
	if(bestMove == -1)
	{
		int bestScore = -999;
		for (int i = 0; i < numMoves; i++) {
			
	        int netScore = scoreAfterMove[i] - opponentResponseScore[i];
	        if (netScore > bestScore) {
	            bestScore = netScore;
	            bestMove = moveIndex[i];
	            //printf("\n ----------> Using highest score\n"); 
	        }
    	}
	}
	
    // Adjusted Joker usage logic
    int myScore = !turn ? scoreA : scoreB;
    int opponentScore = !turn ? scoreB : scoreA;
    int scoreDifference = abs(myScore - opponentScore);
    int movesLeft = GAMESIZE * GAMESIZE - movesMade;

    if (scoreDifference > 70 && movesLeft < GAMESIZE * 4 && bestScore >= 10) {
        shouldUseJoker = ((!turn && jokerA > 0) || (turn && jokerB > 0));
    }

    if (shouldUseJoker) return '*';

 	if (bestMove != -1) {
    	return 65 + bestMove; // Return the best move
	}

	/*
	* If no best move is found, select randomly from available moves even by 
	* second strategy - most avilable score - we must select one avilable cell randomly
	*/
	int availableMoves[GAMESIZE];
	int availableCount = 0;
	
	for (int i = 0; i < GAMESIZE; i++) {
	    if ((!turn && !used[selectible][i]) || (turn && !used[i][selectible])) {
	        availableMoves[availableCount++] = i;
	    }
	}
	
	if (availableCount > 0) {
	    int randomIndex = rand() % availableCount;
	    return 65 + availableMoves[randomIndex]; // Return a random available move
	} else {
	    // Fallback scenario: Should not normally happen, but handle gracefully
	    for (int i = 0; i < GAMESIZE; i++) {
	        if ((!turn && !used[selectible][i]) || (turn && !used[i][selectible])) {
	            return 65 + i; // Return the first available move
	        }
	    }
	}
}

