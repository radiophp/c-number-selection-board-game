
# Competitive Number Selection Game

## Description
This project is a C++ implementation of a competitive number selection game. In this game, players (either human or computer-controlled) take turns selecting numbers from a grid. Each player aims to accumulate the highest possible score by selecting numbers, with the option of using a strategic "Joker" move in critical situations. The game features dynamic strategy adjustments, making the computer player adapt its tactics based on the current state of the game.

## Features
- Player vs Player and Player vs Computer modes.
- Dynamic strategy adjustment for the computer player.
- Joker feature for critical game moments.
- Command-line interface for game interaction.
## Algorithm Description 
	 
AutoPlayer Function
Developed by Mohammad Inanloo Tayefe Yaghmorloo, Student Number B2210.03313

Project Repository: https://gitlab.com/radiophp/c-number-selection-board-game
	 
The AutoPlayer function is designed for a board game, where it automatically selects the best possible move
for the current player (either A or B). The function uses a sophisticated algorithm to analyze the game board
and make strategic decisions.
	 
Algorithm:
1. Initialization: Establish initial variables for best score, best move, joker usage, and count the number 
   of moves already made.

2. Move Evaluation: For each potential move, the function checks whether the cell is available and evaluates
   the potential score of making that move.

   - For Player A (row selector), it evaluates each column of the selectable row.
   - For Player B (column selector), it evaluates each row of the selectable column.

   The function also estimates the best possible response of the opponent to the move and stores these values.

3. Decision Making: The function iterates through all evaluated moves, prioritizing moves with positive scores.
   It calculates a net score for each move by considering the opponent's potential response, aiming to maximize
   the player's score while minimizing the opponent's opportunities.

   If no ideal move (net score is significantly positive) is found, it selects the move with the highest available
   score.

4. Joker Strategy: The function employs a strategic use of the Joker (a special move), particularly when the
   opponent is significantly ahead, a high-value move is available, and the game is close to the end.

5. Random Selection as Fallback: If no move stands out as the best, the function will randomly select from
   the available moves. This ensures that the function always returns a valid move, maintaining the flow of the game.

This algorithmic approach allows for a dynamic and adaptive strategy, making the AutoPlayer function
a competent player in the board game.


## Getting Started
### Prerequisites
- A C++ compiler (such as GCC or Clang)
- Basic knowledge of running C++ programs from the command line

### Installation
1. Clone the repository or download the source code.
2. Compile the code using a C++ compiler. For example:
   ```bash
   g++ -o Game Game.cpp
   ```

### Usage
Run the compiled program from the command line:
```bash
./Game
```
Follow the on-screen prompts to play the game. Choose between human or computer players and select numbers from the grid to accumulate your score.

## Contributing
Contributions to this project are welcome. Please ensure to update tests as appropriate.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.

## Acknowledgements
- This project was created as part of a programming assignment.
- Special thanks to everyone who contributed to the development and testing of this game.
