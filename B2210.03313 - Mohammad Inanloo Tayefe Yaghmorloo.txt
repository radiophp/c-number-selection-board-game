//Mohammad Inanloo Tayefe Yaghmorloo
//B2210.03313

char AutoPlayer() {
	/**
	 * AutoPlayer Function
	 * Developed by Mohammad Inanloo Tayefe Yaghmorloo, Student Number B2210.03313
	 * 
	 * Project Repository: https://gitlab.com/radiophp/c-number-selection-board-game
	 *
	 * The AutoPlayer function is designed for a board game, where it automatically selects the best possible move
	 * for the current player (either A or B). The function uses a sophisticated algorithm to analyze the game board
	 * and make strategic decisions.
	 *
	 * Algorithm:
	 * 1. Initialization: Establish initial variables for best score, best move, joker usage, and count the number 
	 *    of moves already made.
	 * 
	 * 2. Move Evaluation: For each potential move, the function checks whether the cell is available and evaluates
	 *    the potential score of making that move.
	 * 
	 *    - For Player A (row selector), it evaluates each column of the selectable row.
	 *    - For Player B (column selector), it evaluates each row of the selectable column.
	 * 
	 *    The function also estimates the best possible response of the opponent to the move and stores these values.
	 * 
	 * 3. Decision Making: The function iterates through all evaluated moves, prioritizing moves with positive scores.
	 *    It calculates a net score for each move by considering the opponent's potential response, aiming to maximize
	 *    the player's score while minimizing the opponent's opportunities.
	 * 
	 *    If no ideal move (net score is significantly positive) is found, it selects the move with the highest available
	 *    score.
	 * 
	 * 4. Joker Strategy: The function employs a strategic use of the Joker (a special move), particularly when the
	 *    opponent is significantly ahead, a high-value move is available, and the game is close to the end.
	 * 
	 * 5. Random Selection as Fallback: If no move stands out as the best, the function will randomly select from
	 *    the available moves. This ensures that the function always returns a valid move, maintaining the flow of the game.
	 * 
	 * This algorithmic approach allows for a dynamic and adaptive strategy, making the AutoPlayer function
	 * a competent player in the board game.
	 */
	
    int bestScore = -999;
    int bestMove = -1;
    bool shouldUseJoker = false;
    
	// Count the number of moves already made or numbers left on the board
    int movesMade = 0;
    for (int i = 0; i < GAMESIZE; i++) {
        for (int j = 0; j < GAMESIZE; j++) {
            if (used[i][j]) {
                movesMade++;
            }
        }
    }
    
	const int maxMoves = GAMESIZE; // Assuming GAMESIZE is the maximum number of moves
    int moveIndex[maxMoves];
    int scoreAfterMove[maxMoves];
    int opponentResponseScore[maxMoves];
    int numMoves = 0;

    // Evaluate all potential moves and their immediate scores
    for (int i = 0; i < GAMESIZE; i++) {
     
        bool cellIsAvailable = (!turn) ? (!used[selectible][i] && isSelectible(selectible, i)) 
                               : (!used[i][selectible] && isSelectible(i, selectible));
        
        if (cellIsAvailable) {
            int potentialScore = (!turn) ? board[selectible][i] : board[i][selectible];
            int opponentBestResponse = 0;
		//	printf("\nselectible %d used[selectible][i]  %d %d",selectible, i, used[selectible][i]);
            // Estimate the opponent's best response for this move
            for (int j = 0; j < GAMESIZE; j++) {
                if (!turn && !used[j][i]) {
                    // For Player A, check the column for the opponent's best move
                   
                        opponentBestResponse = std::max(opponentBestResponse, board[j][i]);
                 
                } else if(turn && !used[i][j]) {
                    // For Player B, check the row for the opponent's best move
                     
                        opponentBestResponse = std::max(opponentBestResponse, board[i][j]);
                  
                }
            }

            moveIndex[numMoves] = i;
            scoreAfterMove[numMoves] = potentialScore;
            opponentResponseScore[numMoves] = opponentBestResponse;
            numMoves++;
        }    	
    }

    // Evaluate each move option
	for (int i = 0; i < numMoves; i++) {
		//	printf("\nCheck movessss %d numMoves %d",scoreAfterMove[i],numMoves);
	    if (scoreAfterMove[i] > 0) { // Prioritize positive scores
	        int netScore = scoreAfterMove[i] - (opponentResponseScore[i] / 2); // Lessen the impact of opponent's response
	       // Print decision-making information for debugging
        //	printf("\nMove Index: %d, Potential Score: %d, Opponent Response: %d, Net Score: %d", moveIndex[i], scoreAfterMove[i], opponentResponseScore[i], netScore);
	        if (netScore > bestScore) {
	            bestScore = netScore;
	            bestMove = moveIndex[i];
	    //        printf(" --> New Best Move\n");
	        }else {
          //  	printf("\n");
        	}
	    }
	}
	
	// If there is no best move just use most avilable score
	if(bestMove == -1)
	{
		int bestScore = -999;
		for (int i = 0; i < numMoves; i++) {
			
	        int netScore = scoreAfterMove[i] - opponentResponseScore[i];
	        if (netScore > bestScore) {
	            bestScore = netScore;
	            bestMove = moveIndex[i];
	            //printf("\n ----------> Using highest score\n"); 
	        }
    	}
	}
	
    // Adjusted Joker usage logic
    int myScore = !turn ? scoreA : scoreB;
    int opponentScore = !turn ? scoreB : scoreA;
    int scoreDifference = abs(myScore - opponentScore);
    int movesLeft = GAMESIZE * GAMESIZE - movesMade;

    if (scoreDifference > 70 && movesLeft < GAMESIZE * 4 && bestScore >= 10) {
        shouldUseJoker = ((!turn && jokerA > 0) || (turn && jokerB > 0));
    }

    if (shouldUseJoker) return '*';

 	if (bestMove != -1) {
    	return 65 + bestMove; // Return the best move
	}

	/*
	* If no best move is found, select randomly from available moves even by 
	* second strategy - most avilable score - we must select one avilable cell randomly
	*/
	int availableMoves[GAMESIZE];
	int availableCount = 0;
	
	for (int i = 0; i < GAMESIZE; i++) {
	    if ((!turn && !used[selectible][i]) || (turn && !used[i][selectible])) {
	        availableMoves[availableCount++] = i;
	    }
	}
	
	if (availableCount > 0) {
	    int randomIndex = rand() % availableCount;
	    return 65 + availableMoves[randomIndex]; // Return a random available move
	} else {
	    // Fallback scenario: Should not normally happen, but handle gracefully
	    for (int i = 0; i < GAMESIZE; i++) {
	        if ((!turn && !used[selectible][i]) || (turn && !used[i][selectible])) {
	            return 65 + i; // Return the first available move
	        }
	    }
	}
}